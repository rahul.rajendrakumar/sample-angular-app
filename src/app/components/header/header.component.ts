import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/pairwise';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  pathUrl: boolean;
  pathArray: any = [
    { 'title': 'Home', 'path': 'home', 'active': false, 'clickNav': false },
    { 'title': 'Contact-Us', 'path': 'contactus', 'active': false, 'clickNav': false },
    { 'title': 'Parent-Child-Page', 'path': 'parent', 'active': false, 'clickNav': false },
    { 'title': 'Form-Page', 'path': 'form', 'active': false, 'clickNav': true },
    { 'title': 'Directives', 'path': 'directive', 'active': false, 'clickNav': true },
    { 'title': 'Angular-Redux', 'path': 'angularredux', 'active': false, 'clickNav': true },
    { 'title': 'Guards', 'path': 'guards', 'active': false, 'clickNav': true },
    { 'title': 'Lazy Loading', 'path': 'lazyloading', 'active': false, 'clickNav': true },
  ];

  constructor(public router: Router) {
  }

  ngOnInit() {
    this.pathUrl = true;
    this.pathArray[0].active = true;
  }

  /**
   * Page navigation via click funation
   * @param path - path for the navigate which is avaialble in app.routing.ts file
   */
  public navigateTo(path, index) {
    if (path !== '') {
      this.router.navigateByUrl(path);
    }
    // making active true/false based on click
    this.pathArray[index].active = true;
    this.pathArray.forEach((item, itemIndex) => {
      if (itemIndex !== index) {
        item['active'] = false;
      }
    });
  }

}
