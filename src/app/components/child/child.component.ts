import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

  @Input() public message: any;
  @Output() public childmessage: any = new EventEmitter();

  constructor() { }

  ngOnInit() {
      this.childmessage.emit('This is the message from child');
  }

}
