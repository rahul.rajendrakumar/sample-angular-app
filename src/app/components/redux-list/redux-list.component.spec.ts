import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReduxListComponent } from './redux-list.component';

describe('ReduxListComponent', () => {
  let component: ReduxListComponent;
  let fixture: ComponentFixture<ReduxListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReduxListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReduxListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
