import { Component, OnInit } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from '../../services/angular-redux-materials/store';
import { ADD_TODO, REMOVE_TODO, TOGGLE_TODO } from '../../services/angular-redux-materials/action';
import { ITodo } from '../../services/angular-redux-materials/todo';

@Component({
  selector: 'app-redux-list',
  templateUrl: './redux-list.component.html',
  styleUrls: ['./redux-list.component.css']
})
export class ReduxListComponent implements OnInit {

  @select() todos;

  model: ITodo = {
    id: 0,
    description: '',
    responsible: '',
    priority: 'low',
    isCompleted: false
  };

  constructor(private ngRedux: NgRedux<IAppState>) { }

  ngOnInit() {
  }

  toggleTodo(todo) {
    this.ngRedux.dispatch({ type: TOGGLE_TODO, id: todo.id });
  }
}
