import { Component, OnInit } from '@angular/core';
import { NgRedux, select } from '@angular-redux/store';
import { IAppState } from '../../services/angular-redux-materials/store';
import { REMOVE_ALL_TODOS } from '../../services/angular-redux-materials/action';
import { ITodo } from '../../services/angular-redux-materials/todo';
import { ADD_TODO, REMOVE_TODO, TOGGLE_TODO } from '../../services/angular-redux-materials/action';

@Component({
  selector: 'app-redux-create',
  templateUrl: './redux-create.component.html',
  styleUrls: ['./redux-create.component.css']
})
export class ReduxcreateComponent implements OnInit {

  @select() todos;
  @select() lastUpdate;
  model: ITodo = {
    id: 0,
    description: '',
    responsible: '',
    priority: 'low',
    isCompleted: false
  };

  constructor(private ngRedux: NgRedux<IAppState>) { }

  ngOnInit() {
  }

  clearTodos() {
    this.ngRedux.dispatch({ type: REMOVE_ALL_TODOS });
  }

  onSubmit() {
    this.ngRedux.dispatch({ type: ADD_TODO, todo: this.model });
  }

  removeTodo(todo) {
    this.ngRedux.dispatch({ type: REMOVE_TODO, id: todo.id });
  }

}
