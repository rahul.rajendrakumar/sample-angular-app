import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactusComponent } from './modules/contactus/contactus.component';
import { HomeComponent } from './components/home/home.component';
import { ParentComponent } from './modules/parent/parent.component';
import { FormComponent } from './modules/form/form.component';
import { DirectivesComponent } from './components/directives/directives.component';
import { ReduxComponent } from './components/redux/redux.component';
import { GuardComponent } from './modules/guard/guard.component';

/** Routes object that contains each route definition in HitList */
const appRoutes: Routes = [
  { path: 'lazyloading', loadChildren: 'app/modules/lazyload/lazyload.module#LazyloadModule' },
  { path: 'guards', component: GuardComponent },
  { path: 'angularredux', component: ReduxComponent },
  { path: 'directive', component: DirectivesComponent },
  { path: 'form', component: FormComponent },
  { path: 'parent', component: ParentComponent },
  { path: 'contactus', component: ContactusComponent },
  { path: 'home', component: HomeComponent },
  { path: '', component: HomeComponent },
];

/**
 * The AppRoutingModule
 *
 * Contains all Routing information for the HitList application.
 * We use hashes in URLs to fix problems with refreshing, and individual URL targeting.
 */
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, { useHash: true })
  ],
  exports: [
    RouterModule
  ],
  providers: [
  ]
})
export class AppRoutingModule { }
