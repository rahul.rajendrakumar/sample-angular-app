import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[changeTextColor]'
})
export class TextColorChangeDirective {

  constructor(element: ElementRef) {
    element.nativeElement.style.color = 'red';
  }

}
