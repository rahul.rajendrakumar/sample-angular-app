import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[changeText]'
})
export class TextChangeDirective {

  constructor(element: ElementRef) {
    element.nativeElement.innerText = 'Text is changed by changeText Directive.';
  }

}
