import { Component, OnInit, ViewChild, AfterViewInit, AfterContentInit , ChangeDetectorRef } from '@angular/core';
import { ChildComponent } from '../../components/child/child.component';

@Component({
  selector: 'app-parent',
  templateUrl: './parent.component.html',
  styleUrls: ['./parent.component.css']
})
export class ParentComponent implements OnInit, AfterViewInit {
  message: any = 'This message from parent';
  childmessage: any;
  @ViewChild(ChildComponent) childComponent: ChildComponent;

  constructor(private cd: ChangeDetectorRef) { }

  ngOnInit() {

  }


  ngAfterViewInit() {
    setTimeout(() => {
      this.childComponent.message = 'Message passed using View Child from parent to child';
      this.cd.detectChanges();
    }, 500);
  }


  /**
   * Function for child output emitter
   */
  public childmessages(evt) {
    this.childmessage = evt;
  }

}
