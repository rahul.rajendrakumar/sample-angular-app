import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LazyloadComponent } from './lazyload.component';
import { LazyloadRouter } from './lazyload.router';

@NgModule({
  imports: [
    CommonModule,
    LazyloadRouter
  ],
  declarations: [
    LazyloadComponent
  ]
})
export class LazyloadModule { }
