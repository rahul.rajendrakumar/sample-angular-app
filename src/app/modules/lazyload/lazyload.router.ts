import { Routes, RouterModule } from '@angular/router';
import { LazyloadComponent } from './lazyload.component';

const LAZYLOAD_ROUTER: Routes = [{
  path: '',
  component: LazyloadComponent
}];

export const LazyloadRouter = RouterModule.forChild(LAZYLOAD_ROUTER);