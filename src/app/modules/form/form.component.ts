import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  myForm: FormGroup;

  constructor() { }

  ngOnInit() {
    this.setFormControls();
  }

  /**
   * Reactive Form control initialisation
   */
  public setFormControls() {
    this.myForm = new FormGroup({
      name: new FormControl('Benedict', [Validators.required]),
      email: new FormControl(''),
      message: new FormControl('')
    });
  }

  /**
   * Form sumit button click method
   */
  public submitForm() {
    console.log('Valid?', this.myForm.valid); // true or false
    console.log('Name', this.myForm.value.name);
    console.log('Email', this.myForm.value.email);
    console.log('Message', this.myForm.value.message);
  }

}
