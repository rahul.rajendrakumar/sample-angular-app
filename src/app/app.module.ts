import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// components import
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { DirectivesComponent } from './components/directives/directives.component';
import { ReduxcreateComponent } from './components/redux-create/redux-create.component';
import { ReduxListComponent } from './components/redux-list/redux-list.component';
import { ReduxComponent } from './components/redux/redux.component';

// module files import
import { AppRoutingModule } from './app-routing.module';
import { ContactusModule } from './modules/contactus/contactus.module';
import { ParentModule } from './modules/parent/parent.module';
import { FormModule } from './modules/form/form.module';
import { FormsModule } from '@angular/forms';
import { GuardModule } from './modules/guard/guard.module';

// directives file import
import { TextColorChangeDirective } from './directives/text-color-change/text-color-change.directive';
import { TextChangeDirective } from './directives/text-change/text-change.directive';

// service files import
import { AuthService } from './services/auth/auth.service';

// others
import { NgRedux, NgReduxModule } from '@angular-redux/store';
import { IAppState, rootReducer, INITIAL_STATE } from './services/angular-redux-materials/store';



@NgModule({
  // for components
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    TextColorChangeDirective,
    DirectivesComponent,
    TextChangeDirective,
    ReduxComponent,
    ReduxcreateComponent,
    ReduxListComponent
  ],
  // for modules
  imports: [
    BrowserModule,
    AppRoutingModule,
    ContactusModule,
    ParentModule,
    FormModule,
    NgReduxModule,
    FormsModule,
    GuardModule
  ],
  // for services
  providers: [
    AuthService
  ],
  // for first loading component
  bootstrap: [AppComponent]
})

export class AppModule {
  constructor(ngRedux: NgRedux<IAppState>) {
    ngRedux.configureStore(rootReducer, INITIAL_STATE);
  }
}
